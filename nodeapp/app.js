//Declare dependencies
const express = require("express");
const app = express();
const mongoose = require("mongoose");



//Connect to DB
mongoose.connect("mongodb://localhost:27017/mern2_tracker", 
{useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false}); //DeprecationWarning

mongoose.connection.once("open", () => {
	console.log("Now connected to local MongoDB");
});

//Apply Middleware (auth, validation) Request response pipeline
app.use(express.json());
app.use(express.urlencoded({extended: true}));


//Declare Models
const Team = require("./models/teams");
const Task = require("./models/tasks");
const Member = require("./models/members");

//Create Routes/Endpoints
//1.) Create
app.post("/teams", (req, res) => {
	// console.log("test");
	// return res.send(req.body);
	const team = new Team(req.body);
	//save to database
	team.save()
		.then(() => {
			res.send(team)
		})
		.catch((e) => {
			//BAD REQUEST (http response status codes)
			res.status(400).send(e)
		})
});
//2.) Get all
app.get("/teams", (req, res) => {
	// return res.send("get all teams");
	Team.find()
		.then((teams) => {
			return res.status(200).send(teams)
		})
		.catch((e) => {
			return res.status(500).send(e)
		})
});
//3.) Get One
app.get("/teams/:id", (req, res) => {
	// return res.send("get a team");
	// console.log(req.params.id)
	const _id = req.params.id

	//Mongoose Models Query
	Team.findById(_id)
		.then((team) =>{
			if(!team) {
				return res.status(404).send(e)
			}
			return res.send(team)
		})
		.catch((e) => {
			return res.status(500).send(e)
		})
});
//4.) Update One
app.patch("/teams/:id", (req, res) => {
	// return res.send("update a team");
	const _id = req.params.id

	//Mongoose Models Query
	Team.findByIdAndUpdate(_id, req.body, { new:true })
		.then((team) => {
			if(!team) {
				//NOT FOUND
				return res.status(404).send(e)
			}
			return res.send(team)
		})
		.catch((e) => {
			return res.status(500).send(e)
		})
});

//5.) Delete One
app.delete("/teams/:id", (req, res) => {
	// return res.send("delete a team");
	const _id = req.params.id

	//Mongoose Models Query
	Team.findByIdAndDelete(_id)
		.then((team) => {
			if(!team) {
				return res.send(404).send(e)
			}
			return res.send(team)
		})
		.catch((e) => {
			return res.status(500).send(e)
		})
});
//Initialize the server
app.listen(4000, () => {
	console.log("Now listening to port 4000 :)");
});

